extern crate rug;
extern crate rand;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use rand::RngCore;
use rug::Integer;
use rug::rand::RandState;
use std::collections::HashMap;
use std::env;
use std::io;
use std::net::{TcpListener, TcpStream};
use std::io::prelude::*;
use serde::{Serialize, de::DeserializeOwned};

#[derive(Serialize, Deserialize, Debug)]
struct GlobalData {
    lb: u64,
    ub: u64,
    inc: u64,
    prime: Integer,
    generator: Integer
}

struct FlooredGlobalData {
    real_lb: u64,
    real_ub: u64,
    expected_size: usize
}

fn read_number(to_print: &str) -> u64 {
    let mut potential_number = String::new();
    println!("{}", to_print);
    io::stdin().read_line(&mut potential_number)
        .expect("Failed to read line");
    return potential_number.trim().parse().unwrap();
}

fn create_global_data(mut state: &mut RandState) -> GlobalData {
    let lb = read_number("Input Lower Bound!");
    let ub = read_number("Input Upper Bound!");
    let inc = read_number("Input Increment!");

    let p = calculate_prime(&mut state);
    println!("prime:         {}", p);
    let g = calculate_generator(&p, &mut state);
    println!("generator:     {}", g);

    let global_data_to_send = GlobalData {
        lb,
        ub,
        inc,
        prime: p,
        generator: g
    };
    return global_data_to_send;
}

fn verify_and_transform_global_data(global_data: &GlobalData) -> FlooredGlobalData {
    if global_data.ub < global_data.lb {
        panic!("ub < lb");
    }
    if global_data.inc == 0 {
        panic!("inc == 0");
    }
    let real_lb = (global_data.lb / global_data.inc) * global_data.inc;
    let real_ub = (global_data.ub / global_data.inc) * global_data.inc;

    let expected_size = (((real_ub - real_lb) / global_data.inc) + 1) as usize;
    return FlooredGlobalData {
        real_lb,
        real_ub,
        expected_size
    }
}

fn send_serialized<T: Serialize>(
    stream: &mut TcpStream,
    to_send: &T
) {
    let data = serde_json::to_vec(to_send).unwrap();
    let len = data.len();
    stream.write(&len.to_be_bytes()).unwrap();
    stream.write_all(&data).unwrap();
}

fn receive_serialized<T: DeserializeOwned>(
    stream: &mut TcpStream
) -> T {
    let mut bytes_to_read_buffer : [u8; 8] = [0,0,0,0,0,0,0,0];

    stream.read_exact(&mut bytes_to_read_buffer).unwrap();
    let bytes_to_read : usize = usize::from_be_bytes(bytes_to_read_buffer);
    let mut buffer = vec![0; bytes_to_read];
    stream.read_exact(&mut buffer).unwrap();
    let result : T = serde_json::from_slice(&buffer).unwrap();
    return result;
}

fn run_main() {
    // Prepare global information
    let mut state = RandState::new_mersenne_twister();
    let mut rng = rand::thread_rng();
    let seed = Integer::from(rng.next_u64());
    state.seed(&seed);

    let mut stream = TcpStream::connect("127.0.0.1:8000").expect("connection failed");
    let global_data_to_send = create_global_data(&mut state);
    send_serialized(&mut stream, &global_data_to_send);
    let floored_global_data = verify_and_transform_global_data(&global_data_to_send);
    let g_pow_ki_b_vec : Vec<Integer> = receive_serialized(&mut stream);
    if g_pow_ki_b_vec.len() != floored_global_data.expected_size {
        panic!("Expected size: {}, Actual size: {}", floored_global_data.expected_size, g_pow_ki_b_vec.len())
    }
    let x = read_number(&format!("Insert number between {} and {}", floored_global_data.real_lb, floored_global_data.real_ub));
    if x < floored_global_data.real_lb || floored_global_data.real_ub < x {
        panic!("inserted value is out of bounds");
    }
    let real_element_until = (x / global_data_to_send.inc) * global_data_to_send.inc;
    let rand = Integer::random_bits(global_data_to_send.prime.significant_bits() - 1, &mut state);
    let a = Integer::from(rand);
    let map = create_messages(
        floored_global_data.real_lb,
        floored_global_data.real_ub,
        global_data_to_send.inc,
        real_element_until,
        &mut state,
        &global_data_to_send.prime,
        &global_data_to_send.generator,
        &a,
        &g_pow_ki_b_vec
    );
    send_serialized(&mut stream, &map);
    let challenge : Integer = receive_serialized(&mut stream);
    let response = challenge.pow_mod(&a, &global_data_to_send.prime).unwrap();
    send_serialized(&mut stream, &response);

}

fn create_ki_and_g_pow_ki(
    g_pow_b: &Integer,
    p: &Integer,
    expected_size: usize,
    mut state: &mut RandState
) -> (Vec<Integer>, Vec<Integer>) {
    // Exchange all that information
    let mut ki_vec = Vec::with_capacity(expected_size);
    let mut g_pow_ki_b_vec = Vec::with_capacity(expected_size);
    for _ in 0..expected_size {
        let rand = Integer::random_bits(p.significant_bits() - 1, &mut state);
        let ki = Integer::from(rand);
        g_pow_ki_b_vec.push(g_pow_b.clone().pow_mod(&ki, &p).unwrap());
        ki_vec.push(ki.clone())
    }
    return (ki_vec, g_pow_ki_b_vec);
}

fn run_server() {
    // Prepare global information
    let mut state = RandState::new_mersenne_twister();
    let mut rng = rand::thread_rng();
    let seed = Integer::from(rng.next_u64());
    state.seed(&seed);

    let listener = TcpListener::bind("127.0.0.1:8000").expect("could not start server");

    // accept connections and get a TcpStream
    for connection in listener.incoming() {
        match connection {
            Ok(mut stream) => {
                let global_data : GlobalData = receive_serialized(&mut stream);
                let floored_global_data = verify_and_transform_global_data(&global_data);
                let rand = Integer::random_bits(global_data.prime.significant_bits() - 1, &mut state);
                let b = Integer::from(rand);
                let g_pow_b = global_data.generator.clone().pow_mod(&b, &global_data.prime).unwrap();
                let (ki_vec, g_pow_ki_b_vec) = create_ki_and_g_pow_ki(
                    &g_pow_b,
                    &global_data.prime,
                    floored_global_data.expected_size,
                    &mut state
                );
                send_serialized(&mut stream, &g_pow_ki_b_vec);
                let map : HashMap<u64, (Integer, Integer)> = receive_serialized(&mut stream);
                let x = read_number(&format!("Insert number between {} and {}", floored_global_data.real_lb, floored_global_data.real_ub));
                if x < floored_global_data.real_lb || floored_global_data.real_ub < x {
                    panic!("inserted value is out of bounds");
                }
                let floored_chosen_value = (x / global_data.inc) * global_data.inc;

                let index = ((floored_chosen_value - floored_global_data.real_lb) / global_data.inc) as usize;
                let chosen_entry = map.get(&floored_chosen_value).unwrap();
                let (b_hint, b_cipher_message) = chosen_entry;
                let challenge = b_hint.clone().pow_mod(&ki_vec[index], &global_data.prime).unwrap();
                send_serialized(&mut stream, &challenge);
                let response: Integer = receive_serialized(&mut stream);
                let to_inverse = response.pow_mod(&b, &global_data.prime).unwrap();
                let inverse = to_inverse.invert(&global_data.prime).unwrap();
                let message = (b_cipher_message * inverse) % &global_data.prime;
                println!("result:        {}", message);
                if message.get_bit(0) {
                    println!("Your value is less or equal");
                } else {
                    println!("Your value is higher");
                }

            }
            Err(e) => { println!("connection failed {}", e); }
        }
    }
}


fn main() {
    if env::args().count() > 1 {
        run_main();
    } else {
        run_server();
    }
}

fn create_messages(
    lb: u64,
    ub: u64,
    inc: u64,
    element_until: u64,
    mut state: &mut RandState,
    p: &Integer,
    g: &Integer,
    a: &Integer,
    g_pow_ki_b_vec: &Vec<Integer>
) -> HashMap<u64, (Integer, Integer)> {
    let mut result = HashMap::new();
    for i in (lb .. ub).step_by(inc as usize) {
        let index = ((i - lb) / inc) as usize;
        let rand = Integer::random_bits(p.significant_bits() - 1, &mut state);
        let mut message = Integer::from(rand);
        if i <= element_until && !message.get_bit(0) ||
            element_until < i && message.get_bit(0)
        {
            message += 1;
        }
        let rand = Integer::random_bits(p.significant_bits() - 1, &mut state);
        let r_i = Integer::from(rand);

        let hint = g.clone().pow_mod(&r_i, &p).unwrap();
        let factor = g_pow_ki_b_vec[index].clone().pow_mod(&(r_i * a), &p).unwrap();
        if i == element_until {
            println!("message:       {}", message);
        }
        result.insert(i, (hint, (message * factor) % p));
    }

    return result;
}

fn calculate_generator(
    p: &Integer,
    mut state: &mut RandState
) -> Integer {
    let q = Integer::from(p - 1) / 2;
    let two = Integer::from(2u64);
    loop {
        let rand = Integer::random_bits(p.significant_bits() - 1, &mut state);
        let mut num = Integer::from(rand);
        if Integer::from(&num % 2) == 0 {
            num += 1;
        }
        match num.clone().pow_mod(&two, &p) {
            Err(_) => continue,
            Ok(value) => if value == 1 {
                continue;
            }
        }

        match num.clone().pow_mod(&q, &p) {
            Err(_) => continue,
            Ok(value) => if value == 1 {
                continue;
            }
        }

        return num;
    }
}

fn calculate_prime(mut state: &mut RandState) -> Integer {
    loop {
        let rand = Integer::random_bits(256, &mut state);
        let num = Integer::from(rand);
        let q = num.next_prime();
        let possible_p : Integer = (q * 2) + 1;
        if possible_p.is_probably_prime(15) != rug::integer::IsPrime::No {
            return possible_p;
        }
    }
}
